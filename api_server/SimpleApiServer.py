from http.server import BaseHTTPRequestHandler, HTTPServer
import re
from threading import Thread
import requests
from src.config import *


class SimpleApiServer(BaseHTTPRequestHandler):
    def set_response(self, response_content):
        self.send_response(requests.codes.ok)
        self.send_header('Content-Type', 'application/json; charset=utf-8')
        self.end_headers()
        response = json.dumps(response_content)
        response = bytes(response, 'utf-8')
        self.wfile.write(response)

    def do_GET(self):
        if re.search(Config.endpoints()["user_1"], self.path):
            response_content = Config.response_get_user_1().response
            self.set_response(response_content)
        if re.search(Config.endpoints()["user_2"], self.path):
            self.send_response(404, 'user not found')
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

    def do_POST(self):
        if re.search(Config.endpoints()["post_create"], self.path):
            response_content = Config.response_post().response
            self.set_response(response_content)


def start_mock_server(port):
    mock_server = HTTPServer(('0.0.0.0', port), SimpleApiServer)
    mock_server_thread = Thread(target=mock_server.serve_forever())
    mock_server_thread.setDaemon(True)
    mock_server_thread.start()


start_mock_server(5050)
