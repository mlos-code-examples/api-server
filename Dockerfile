FROM python:3.8-slim-buster

WORKDIR /api_server
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN apt-get update
RUN apt-get install -y curl
COPY . .
EXPOSE 5050
HEALTHCHECK --interval=5s --timeout=5s CMD curl -s http://localhost:5050/users/1 || exit 1
ENTRYPOINT ["python", "-u", "api_server/SimpleApiServer.py"]